﻿using rrepo;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using rrepo.Logic;

namespace rrepo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.ConfigureAppConfiguration((hostingEnv, config) => {
                //    config
                //        .AddInMemoryCollection(
                //            new Dictionary<string, string>() {
                //                { "database_password", SecretDialog.GetUserSecret("Enter password: ") }
                //            }
                //        );
                //})
                .UseStartup<Startup>()
                //.UseUrls("http://+:17460")
                .Build();
    }
}
