﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace rrepo.Models.DataForm
{
    public enum QueryType
    {
        Repos,
        Users
    }

    public class SearchModel
    {
        [Range(1, int.MaxValue)]
        public string Query { get; set; }

        [Required]
        public QueryType QueryType { get; set; }
    }
}
