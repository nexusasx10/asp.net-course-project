using System.ComponentModel.DataAnnotations;

namespace rrepo.Models.DataForm
{
    public class NewRepoDF
    {
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}