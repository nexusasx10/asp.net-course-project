using System.ComponentModel.DataAnnotations;

namespace rrepo.Models.DataForm
{
    public class RegistrationDFM
    {
        [MinLength(3)]
        [MaxLength(50)]
        public string Login { get; set; }

        [MinLength(3)]
        [MaxLength(50)]
        public string EMail { get; set; }

        [MinLength(8)]
        [MaxLength(50)]
        public string Password { get; set; }
    }
}