using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rrepo.Models.DataBase
{
    public class AccountDBM
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MinLength(3)]
        [MaxLength(50)]
        public string Login { get; set; }

        [MinLength(5)]
        [MaxLength(50)]
        public string EMail { get; set; }

        [MinLength(8)]
        [MaxLength(50)]
        public string Password { get; set; }

        public string OAuthToken { get; set; }

        public List<RepositoryDBM> Repositories { get; set; }
        
        public AccountDBM()
        {
            Repositories = new List<RepositoryDBM>();
        }
    }
}
