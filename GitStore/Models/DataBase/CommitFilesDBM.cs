using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rrepo.Models.DataBase
{
    public class CommitFilesDBM
    {
        [Required]
        public int CommitID { get; set; }
        public CommitDBM Commit { get; set; }

        [Required]
        public int FileID { get; set; }
        public GitFileDBM File { get; set; }
    }
}
