using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rrepo.Models.DataBase
{
    public class GitFileDBM
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [DataType("char(20)")]
        public string SHA1 { get; set; }

        public List<CommitFilesDBM> CommitFiles { get; set; }

        public GitFileDBM()
        {
            CommitFiles = new List<CommitFilesDBM>();
        }
    }
}