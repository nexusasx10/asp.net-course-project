﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rrepo.Models.DataBase
{
    public class LanguageDBM
    {
        [Key]
        public int Id { get; set; }

        public string Language { get; set; }

        public int RepositoryId { get; set; }
    }
}
