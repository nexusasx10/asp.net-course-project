using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rrepo.Models.DataBase
{
    public class RepositoryDBM
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public int OwnerID { get; set; }
        
        [MaxLength(300)]
        public string Description { get; set; }

        public List<CommitDBM> Commits { get; set; }

        public AccountDBM Owner { get; set; }

        public RepositoryDBM()
        {
            Commits = new List<CommitDBM>();
        }
    }
}