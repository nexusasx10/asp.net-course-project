using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rrepo.Models.DataBase
{
    public class CommitDBM
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public int RepositoryID { get; set; }
        public RepositoryDBM Repository { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Author { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(200)]
        public string Comment { get; set; }

        [Required]
        public DateTime Time { get; set; }

        public List<CommitFilesDBM> CommitFiles { get; set; }

        public CommitDBM()
        {
            CommitFiles = new List<CommitFilesDBM>();
        }
    }
}
