﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rrepo.Models.Config
{
    public class VkOAuth
    {
        public string AppId { get; set; }

        public string PrivateKey { get; set; }
    }
}
