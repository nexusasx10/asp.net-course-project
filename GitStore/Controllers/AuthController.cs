﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using rrepo.Logic;
using rrepo.Models.Config;
using rrepo.Models.DataBase;
using rrepo.Models.DataForm;

namespace rrepo.Controllers
{
    public class AuthController : Controller
    {
        private async void Authenticate(AuthenticationDFM model)
        {
            var claims = new List<Claim>() {
                new Claim(ClaimsIdentity.DefaultNameClaimType, model.Login)
            };
            var id = new ClaimsIdentity(
                claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType
            );
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/Home/Info?accName=" + User.Identity.Name);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromServices] DataContext _context, AuthenticationDFM model)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/Home/Info?accName=" + User.Identity.Name);
            if (_context.Accounts.FirstOrDefault(acc => (acc.Login == model.Login) && acc.Password.Equals(model.Password)) != null)
            {
                Authenticate(model);
                return Redirect("/Home/Index");
            }
            ViewData["isLoginError"] = "Неверный логин и(или) пароль";
            return View();
        }

        [HttpGet]
        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Registration([FromServices] DataContext _context, RegistrationDFM model)
        {
            if (_context.Accounts.FirstOrDefault(user => user.Login == model.Login) != null)
            {
                ModelState.AddModelError("user_exists_error", "Пользователь с таким логином уже существует");
                return RedirectToAction("Registration");
            }
            _context.Accounts.Add(new AccountDBM() { Login = model.Login, EMail = model.EMail, Password = model.Password });
            _context.SaveChanges();
            var authModel = new AuthenticationDFM() { Login = model.Login, Password = model.Password };
            Authenticate(authModel);
            return Redirect("/Home/Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> OAuth([FromServices] DataContext _context, string code, string access_token, string user_id)
        {
            if (access_token != null)
            {
                if (_context.Accounts.FirstOrDefault(user => user.OAuthToken == access_token) == null)
                {
                    _context.Accounts.Add(new AccountDBM() { Login = $"vk{user_id}", OAuthToken = access_token });
                    _context.SaveChanges();
                }
                var model = new AuthenticationDFM() { Login = $"vk{user_id}", Password = access_token };
                Authenticate(model);
                return Redirect("/Home/Index");
            }
            if (code == null)
                return RedirectToAction("Registration");
            else
                return Redirect($"https://oauth.vk.com/access_token?client_id=6797152&client_secret=MtzdiL0Ye7d5VmtInr5t&redirect_uri=https://gitstore.azurewebsites.net/Auth/OAuth&code={code}");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }
    }
}
