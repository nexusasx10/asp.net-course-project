﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using rrepo.Logic;
using rrepo.Models.DataBase;
using rrepo.Models.DataForm;

namespace rrepo.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Init([FromServices] DataContext _context)
        {
            Adder.Fill(_context);
            return Redirect("/Home/Index");
        }
        
        [HttpGet]
        public IActionResult NewRepo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult NewRepo([FromServices] DataContext _context, NewRepoDF repo)
        {
            if (User.Identity.IsAuthenticated)
            {
                var cur = _context.Accounts.Where(x => x.Login == User.Identity.Name).First();
                _context.Repositories.Add(new RepositoryDBM() { Name = repo.Name, Owner = cur, Description=repo.Description });
                _context.SaveChanges();
            }
            return Redirect("/Home/Index");
        }

        [HttpGet]
        public IActionResult Index([FromServices] DataContext _context)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("/Home/Info?accName=" + User.Identity.Name);
            return Redirect("/Auth/Login");
        }

        [HttpGet]
        public IActionResult Search([FromServices] DataContext _context, SearchModel model)
        {
            if (model.QueryType == QueryType.Repos)
            {
                ViewData["Repos"] = _context.Repositories.Where(repo => repo.Name.Contains(model.Query)).Join(_context.Accounts, repo => repo.OwnerID, user => user.ID, (x, y) => new Tuple<RepositoryDBM, AccountDBM>(x, y));
            }
            if (model.QueryType == QueryType.Users)
            {
                ViewData["Users"] = _context.Accounts.Where(user => user.Login.Contains(model.Query) || user.EMail.Contains(model.Query));
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Info([FromServices] DataContext _context, string accName, string repoName)
        {
            if (string.IsNullOrEmpty(accName))
                return StatusCode(404);
                
            AccountDBM accDB = _context.Accounts.FirstOrDefault(acc => acc.Login == accName);
            if (accDB == null)
                return StatusCode(404);

            if (string.IsNullOrEmpty(repoName))
                return View("UserInfo", accDB);
            
            RepositoryDBM repoDB = _context.Repositories.FirstOrDefault(repo => repo.Owner == accDB);
            if (repoDB == null)
                return StatusCode(404);
            
            return View("RepoInfo", repoDB);
        }
    }
}
