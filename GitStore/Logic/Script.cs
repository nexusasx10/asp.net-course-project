using System.Collections.Generic;
using rrepo.Models.DataBase;

namespace rrepo.Logic
{
    public class Adder
    {
        public static void Fill(DataContext context)
        {
            var accs = new List<AccountDBM>() {
                new AccountDBM() { Login= "Login1", EMail= "EMail1@smth.ru", Password="Password1" },
                new AccountDBM() { Login= "Login2", EMail= "EMail2@smth.ru", Password="Password2" },
                new AccountDBM() { Login= "Login3", EMail= "EMail3@smth.ru", Password="Password3" }
            };
            context.Accounts.AddRange(accs);
            
            var repos = new List<RepositoryDBM>() {
                new RepositoryDBM() { Name= "Name1", Owner=accs[0], Description="Description 1..."},
                new RepositoryDBM() { Name= "Name2", Owner=accs[1], Description="Description 2..."},
                new RepositoryDBM() { Name= "Name3", Owner=accs[2], Description="Description 3..."}
            };
            context.Repositories.AddRange(repos);

            var cms = new List<CommitDBM>() {
                new CommitDBM() { Repository=repos[0], Author="Author1", Comment="Comment1", Time=new System.DateTime()},
                new CommitDBM() { Repository=repos[0], Author="Author2", Comment="Comment2", Time=new System.DateTime()},
                new CommitDBM() { Repository=repos[1], Author="Author3", Comment="Comment3", Time=new System.DateTime()},
                new CommitDBM() { Repository=repos[1], Author="Author3", Comment="Comment4", Time=new System.DateTime()},
                new CommitDBM() { Repository=repos[2], Author="Author4", Comment="Comment5", Time=new System.DateTime()},
                new CommitDBM() { Repository=repos[2], Author="Author5", Comment="Comment6", Time=new System.DateTime()}
            };
            context.Commits.AddRange(cms);

            var files = new List<GitFileDBM>() {
                new GitFileDBM() { Name="Name1", SHA1="15768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name2", SHA1="25768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name3", SHA1="35768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name4", SHA1="45768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name5", SHA1="55768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name6", SHA1="65768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name2", SHA1="75768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name1", SHA1="85768412c1a56912457889562312457889562312" },
                new GitFileDBM() { Name="Name4", SHA1="95768412c1a56912457889562312457889562312" },
            };
            context.Files.AddRange(files);

            var aaa = new List<CommitFilesDBM>() {
                new CommitFilesDBM() { File=files[0], Commit=cms[0] },
                new CommitFilesDBM() { File=files[1], Commit=cms[0] },
                new CommitFilesDBM() { File=files[2], Commit=cms[1] },
                new CommitFilesDBM() { File=files[3], Commit=cms[2] },
                new CommitFilesDBM() { File=files[4], Commit=cms[2] },
                new CommitFilesDBM() { File=files[5], Commit=cms[3] },
                new CommitFilesDBM() { File=files[6], Commit=cms[4] },
                new CommitFilesDBM() { File=files[7], Commit=cms[4] },
                new CommitFilesDBM() { File=files[8], Commit=cms[5] },
            };
            context.CommitFiles.AddRange(aaa);

            var lan = new List<LanguageDBM>() {
                new LanguageDBM() { Language="Python", RepositoryId=1 },
                new LanguageDBM() { Language="C#", RepositoryId=2 },
                new LanguageDBM() { Language="C++", RepositoryId=2 },
                new LanguageDBM() { Language="Python", RepositoryId=3 },
                new LanguageDBM() { Language="Java", RepositoryId=3 }
            };
            context.Languages.AddRange(lan);

            context.SaveChanges();
        }
    }
}
