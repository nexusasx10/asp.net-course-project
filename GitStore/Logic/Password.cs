﻿using System;
using System.Collections.Generic;

namespace rrepo.Logic
{
    public class SecretDialog
    {
        public static string GetUserSecret(string prompt)
        {
            Console.Write(prompt);
            List<char> passwordChars = new List<char>();
            int cursorPosition;
            ConsoleKeyInfo pressedKey = Console.ReadKey(true);
            while (pressedKey.Key != ConsoleKey.Enter)
            {
                if (pressedKey.Key == ConsoleKey.Backspace)
                {
                    if (passwordChars.Count == 0)
                        continue;
                    passwordChars.RemoveAt(passwordChars.Count - 1);
                    cursorPosition = Console.CursorLeft;
                    Console.SetCursorPosition(cursorPosition - 1, Console.CursorTop);
                    Console.Write(" ");
                    Console.SetCursorPosition(cursorPosition - 1, Console.CursorTop);
                }
                else
                {
                    passwordChars.Add(pressedKey.KeyChar);
                    Console.Write("*");
                }
                pressedKey = Console.ReadKey(true);
            }
            Console.WriteLine();
            return String.Join("", passwordChars);
        }
    }
}
