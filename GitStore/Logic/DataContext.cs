using Microsoft.EntityFrameworkCore;
using rrepo.Models.DataBase;
using System;

namespace rrepo.Logic
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            try
            {
                Database.EnsureCreated();
            }
            catch (TimeoutException e)
            {
                Console.WriteLine("Can't get db connecton");
            }
        }

        public DbSet<AccountDBM> Accounts { get; set; }
        public DbSet<RepositoryDBM> Repositories { get; set; }
        public DbSet<CommitDBM> Commits { get; set; }
        public DbSet<GitFileDBM> Files { get; set; }
        public DbSet<LanguageDBM> Languages { get; set; }

        public DbSet<CommitFilesDBM> CommitFiles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AccountDBM>().ToTable("Accounts");
            builder.Entity<RepositoryDBM>().ToTable("Repositories");
            builder.Entity<CommitDBM>().ToTable("Commits");
            builder.Entity<GitFileDBM>().ToTable("Files");
            builder.Entity<LanguageDBM>().ToTable("Languages");
            
            builder.Entity<CommitFilesDBM>().HasKey(cf => new {cf.CommitID, cf.FileID});
            builder.Entity<CommitFilesDBM>()
                .HasOne(cf => cf.Commit)
                .WithMany(c => c.CommitFiles)
                .HasForeignKey(cf => cf.CommitID);
            builder.Entity<CommitFilesDBM>()
                .HasOne(cf => cf.File)
                .WithMany(f => f.CommitFiles)
                .HasForeignKey(cf => cf.FileID);
        }

    }
}